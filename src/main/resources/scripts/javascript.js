/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
/** Sometimes the sized calculated by the original method are incorrect so override it **/
Zen.adjustCanvas = function() {
	if (Zen.isUserFullScreen) {
		Zen.sizes.canvas.width = Zen.$(window).width() - Zen.sizes.canvas.margin;
	}
	Zen.$("#wrapper").width(Zen.sizes.canvas.width);
}

/** Make sure the current menu tab is set to current using the Spacekey **/
AJS.toInit(function() {
	AJS.$("#zen-menu li > a[href$='" + spaceKey + "']").each(function() {
		AJS.$(this).parents().filter("li").addClass("current");
	});
});

/** Make the tab itself clickable **/
AJS.toInit(function() {
	var redirectUrl = undefined;
	AJS.$("#zen-menu li.toolbar-menu-toggle").each(function(index, item) {
		AJS.$(item).unbind("click").click(function(event) {
			event.preventDefault();
			if (redirectUrl === undefined) {
				redirectUrl = AJS.$(item).children("a:first").attr("href");
				document.location.href = redirectUrl;
			}
		})
	});
});

/** Hide the zen toolbar and move the login link **/
AJS.toInit(function() {
	AJS.$("#zen-toolbar #expando").remove();
	AJS.$("#zen-toolbar #pin").remove();
	if (AJS.Meta.get("remote-user") === undefined || AJS.Meta.get("remote-user") === "") {
		AJS.$("#zen-toolbar").addClass("anonymous");
		AJS.$("#toolbar-menu").prependTo(AJS.$("#zen-toolbar"))
		AJS.$("#zen-toolbar #toolbar-menu li[id!='user-menu']").remove();
	} else {
		AJS.$("#zen-toolbar-holder").css("width", "").addClass("full-screen");
		AJS.$("#toolbar-menu .dropdown").hide();
	}
});

/** Increase the height of the comments column when the quickbox is loaded **/
AJS.toInit(function() {
    AJS.bind("init.rte", function() {
        AJS.$("#comments-section").height(AJS.$("#comments-section").height() + parseInt(AJS.Meta.get("min-editor-height")) - 50);
        // When commenting a large text the editor is not resizing correctly and not showing the scrollbars when needed
        AJS.$("#wysiwyg").css({"overflow-x": "auto", "height": "115%"});
    });
});

/** Correct implementation of the resize window method **/
AJS.toInit( function ($) {
	$("#zen-footer p:empty").remove();
	$("#zen-footer-back p").appendTo($("#zen-footer"));
	$("#zen-footer p:first").remove();
	var ensureFooterAtBottom = function () {
		// Adjust the minimum height of the canvas to ensure the footer never floats up from the bottom edge of the window (at least on page load)
		$("#canvas").css({minHeight: $(window).height() - $("#header").outerHeight(true) - $("#zen-footer-back").outerHeight(true) - 
			$("#canvas").outerHeight(true) - $("#zen-toolbar").outerHeight(true) + $("#canvas").height()});
	}
	$(window).resize(ensureFooterAtBottom);
	ensureFooterAtBottom();
});
